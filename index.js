import fs from "fs";

const START_TILE = "S";
const HOME_TILE = "E";
const OBSTACLE_TILE = "#";

class Robot {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.velX = 0;
    this.velY = -1;
    this.steps = 0;
  }

  turnRight = function () {
    if (Math.abs(this.velY) > 0) {
      this.velX = -this.velY;
      this.velY = 0;
      return;
    }
    this.velY = this.velX;
    this.velX = 0;
  };

  moveForward = function () {
    this.x += this.velX;
    this.y += this.velY;
    this.steps++;
  };

  getCoordsInFront = function () {
    return { x: this.x + this.velX, y: this.y + this.velY };
  };
}

const getStartCoordinates = (map) => {
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[0].length; x++) {
      if (map[y][x] === START_TILE) {
        return { x, y };
      }
    }
  }
};

const getTileInFront = (robot, map) => {
  const coords = robot.getCoordsInFront();
  return map[coords.y][coords.x];
};

const isHome = (robot, map) => map[robot.y][robot.x] === HOME_TILE;

const getSteps = (robot, map) => {
  while (!isHome(robot, map)) {
    while (getTileInFront(robot, map) === OBSTACLE_TILE) {
      robot.turnRight();
    }
    robot.moveForward();
  }
  return robot.steps;
};

const file = fs.readFileSync("./map.txt");
const map = file.toString().split("\n");

const { x, y } = getStartCoordinates(map);
const robot = new Robot(x, y);

console.log(getSteps(robot, map));
